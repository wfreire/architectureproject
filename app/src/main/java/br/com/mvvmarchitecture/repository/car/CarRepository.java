//package br.com.mvvmarchitecture.repository.car;
//
//import android.arch.lifecycle.LiveData;
//import android.arch.lifecycle.MediatorLiveData;
//import android.arch.lifecycle.MutableLiveData;
//import android.arch.lifecycle.Observer;
//import android.support.annotation.Nullable;
//
//import java.util.List;
//
//import javax.inject.Inject;
//
//import br.com.mvvmarchitecture.core.util.Resource;
//import br.com.mvvmarchitecture.repository.car.data.entity.Car;
//import br.com.mvvmarchitecture.repository.car.local.CarDao;
//import br.com.mvvmarchitecture.repository.car.remote.CarService;
//import br.com.mvvmarchitecture.repository.core.ApiResponse;
//
//public class CarRepository {
//
//    private CarService carService;
//    private CarDao carDao;
//    private MediatorLiveData<ApiResponse<List<Car>>> result;
//
//    @Inject
//    public CarRepository(CarService carService, CarDao carDao) {
//        this.carService = carService;
//        this.carDao = carDao;
//        result = new MediatorLiveData<>();
//    }
//
//    public MutableLiveData<Resource<List<Car>>> getCars(Boolean shouldForceFetch) {
//        final MutableLiveData newsData = new MutableLiveData<Resource<List<Car>>>();
//        newsData.setValue(Resource.Companion.loading(null));
//
//        LiveData<ApiResponse<List<Car>>> apiResponse = carService.getCars();
//
//        result.addSource(apiResponse, new Observer<ApiResponse<List<Car>>>() {
//            @Override
//            public void onChanged(@Nullable ApiResponse<List<Car>> apiResponse) {
//                if(apiResponse != null && apiResponse.isSuccessful()) {
//                    if(apiResponse.getBody() != null) {
//                        newsData.setValue(Resource.Companion.success(apiResponse.getBody(), false));
//                    }
//                } else {
//                    if(apiResponse.getMessage() != null) {
//                        newsData.setValue(Resource.Companion.error(apiResponse.getMessage(), null));
//                    }
//                }
//            }
//        });
//
//        return newsData;
//    }
//}
//
