package br.com.mvvmarchitecture.repository.car.remote

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import br.com.mvvmarchitecture.repository.car.data.entity.Car
import br.com.mvvmarchitecture.repository.core.ApiResponse
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import javax.inject.Inject

class CarService @Inject constructor(private val carEndPoint: CarEndPoint) {

    fun getCars(): LiveData<ApiResponse<List<Car>>> {
        val newsData = MutableLiveData<ApiResponse<List<Car>>>()
        carEndPoint.getCars().enqueue(object : Callback<List<Car>> {
                override fun onResponse(call: Call<List<Car>>,
                                        response: Response<List<Car>>) {
                    if (response.isSuccessful) {
                        newsData.value = ApiResponse(response)
                    }
                }

                override fun onFailure(call: Call<List<Car>>, t: Throwable) {
                    newsData.value = ApiResponse(t)
                }
            })
        return newsData
    }
}