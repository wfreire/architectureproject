package br.com.mvvmarchitecture.repository

import android.arch.core.util.Function
import android.arch.lifecycle.MediatorLiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.Transformations
import br.com.mvvmarchitecture.core.util.Resource
import br.com.mvvmarchitecture.repository.car.data.entity.Car
import br.com.mvvmarchitecture.repository.car.local.CarDao
import br.com.mvvmarchitecture.repository.car.remote.CarService
import br.com.mvvmarchitecture.repository.core.ApiResponse
import javax.inject.Inject

class CarRepository @Inject constructor(val carService: CarService, val carDao: CarDao) {

        fun getCars(shouldForceFetch: Boolean = false): MutableLiveData<Resource<List<Car>>> {
            var newsData = MutableLiveData<Resource<List<Car>>>()
            newsData.value = Resource.loading(null)

            val apiResponse = carService.getCars()

//        return object : NetworkBoundRepository<List<Car>, List<Car>>() {
//            override fun saveFetchData(cars: List<Car>) {
//                cars.forEach {
//                    carDao.insertCar(it)
//                }
//            }
//
//            override fun shouldFetch(data: List<Car>?): Boolean {
//                return data == null || data.isEmpty() || shouldForceFetch
//            }
//
//            override fun loadFromDb(): LiveData<List<Car>> {
//                val carLiveData = carDao.getAllCars()
//                val data: MutableLiveData<List<Car>> = MutableLiveData()
//                data.value = carLiveData.value
//                return data
//            }
//
//            override fun fetchService(): LiveData<ApiResponse<List<Car>>> {
//                return carService.getCars()
//            }
//
//            override fun onFetchFailed(message: String?) {
//                Log.e("onFetchFailed", message)
//            }
//        }.asLiveData()


//            apiResponse.observeForever { response ->
//                if (response != null && response.isSuccessful) {
//                    if (response.body != null) {
//                        newsData.value = Resource.success(response.body, false)
//                    }
//                } else {
//                    if (response!!.message != null) {
//                        newsData.value = Resource.error(response.message!!, null)
//                    }
//                }
//            }

//           newsData = Transformations.map(apiResponse) {
//                return@map Resource.success(it.body, false)
//            } as MutableLiveData<Resource<List<Car>>>


            newsData = Transformations.map(apiResponse, Function<ApiResponse<List<Car>>,Resource<List<Car>>>() {
              return@Function Resource.success(it.body,false)
            }) as MutableLiveData

            return newsData
    }
}