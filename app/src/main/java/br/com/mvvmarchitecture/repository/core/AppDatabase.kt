package br.com.mvvmarchitecture.repository.core

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import br.com.mvvmarchitecture.repository.car.data.entity.Car
import br.com.mvvmarchitecture.repository.car.local.CarDao

@Database(entities = [(Car::class)], version = 1, exportSchema = false)
abstract class AppDatabase: RoomDatabase() {

    abstract fun carDao(): CarDao
}