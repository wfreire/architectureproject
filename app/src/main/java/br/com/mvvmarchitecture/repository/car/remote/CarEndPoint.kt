package br.com.mvvmarchitecture.repository.car.remote

import br.com.mvvmarchitecture.repository.car.data.entity.Car
import retrofit2.Call
import retrofit2.http.GET

interface CarEndPoint {

    @GET("v1/carro")
    fun getCars(): Call<List<Car>>

    @GET("v1/carro")
    fun getCarById(carId: Int): Call<Car>
}