package br.com.mvvmarchitecture.repository.car.data.entity

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import br.com.mvvmarchitecture.core.base.BaseModel
import com.squareup.moshi.Json

@Entity(tableName = "car")
data class Car(@PrimaryKey @Json(name = "id") val id: Int,
               @Json(name = "nome") val name: String,
               @Json(name = "descricao") val description: String,
               @Json(name = "marca") val brand: String,
               @Json(name = "quantidade") val quantity: String,
               @Json(name = "preco") val price: String,
               @Json(name = "imagem") val image: String) : BaseModel()