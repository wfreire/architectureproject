package br.com.mvvmarchitecture.repository.car.data.domain

data class CarDomain(val id: Int,
                  val name: String,
                  val description: String,
                  val brand: String,
                  val quantity: String,
                  val price: String,
                  val image: String)