package br.com.mvvmarchitecture.repository.core;

import java.util.HashMap;

import javax.inject.Inject;

import br.com.mvvmarchitecture.BuildConfig;
import retrofit2.Retrofit;

public class ApiFactory {

    private HashMap<String, KeyValueRetrofit> apis;
    private Retrofit.Builder retrofitBuilder;

    @Inject
    public ApiFactory(Retrofit.Builder retrofitBuilder) {
        this.retrofitBuilder = retrofitBuilder;
    }

    public <T> T buildApi(String url, Class<T> type) {
        T api;
        String key = url.concat(type.getSimpleName());
//        apis = (apis == null)? new HashMap() : apis;
//        if (apis.containsKey(key) && apis.get(key) != null
//                && apis.get(key).api != null
//                && apis.get(key).url != null
//                && apis.get(key).className != null) {
//            return (T) apis.get(key).api;
//        }
        api = retrofitBuilder.baseUrl(url).build().create(type);
        apis.put(key, new KeyValueRetrofit<>(api, url, type.getSimpleName()));
        return api;
    }

    class KeyValueRetrofit<T> {
        public final String className;
        public final String url;
        public final T api;

        public KeyValueRetrofit(T api, String url, String className) {
            this.api = api;
            this.url = url;
            this.className = className;
        }
    }
}
