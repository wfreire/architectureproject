package br.com.mvvmarchitecture.repository.car.local

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.*
import br.com.mvvmarchitecture.repository.car.data.entity.Car

@Dao
interface CarDao {

    @Query("SELECT * FROM Car")
    fun getAllCars(): LiveData<List<Car>>

    @Query("SELECT * FROM Car WHERE id = :carId")
    fun getCarById(carId: Int): LiveData<Car>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertCar(car: Car)

    @Update
    fun updateCar(car: Car)

    @Delete
    fun deleteCar(car: Car)
}