package br.com.mvvmarchitecture.core.di

import br.com.mvvmarchitecture.feature.main.MainActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivitiesModule {

    @ContributesAndroidInjector
    abstract fun contributesMainActivity(): MainActivity
}