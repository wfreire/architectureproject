package br.com.mvvmarchitecture.core.util

class Resource<out T>(val status: Status, val data: T?, val message: String?, val onLastPage: Boolean) {

  companion object {
    fun <T> success(data: T?, onLastPage: Boolean = false): Resource<T> {
      return Resource(status = Status.SUCCESS, data = data, message = null, onLastPage = false)
    }

    fun <T> error(msg: String, data: T?): Resource<T> {
      return Resource(status = Status.ERROR, data = data, message = msg, onLastPage = true)
    }

    fun <T> loading(data: T?): Resource<T> {
      return Resource(status = Status.LOADING, data = data, message = null, onLastPage = false)
    }
  }
}
