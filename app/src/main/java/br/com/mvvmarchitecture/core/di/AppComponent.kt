package br.com.mvvmarchitecture.core.di

import br.com.mvvmarchitecture.MvvmArchApplication
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(
        modules = [
            (AndroidSupportInjectionModule::class),
            (ApplicationModule::class),
            (ViewModelModule::class),
            (ActivitiesModule::class),
            (FragmentModule::class),
            (NetworkModule::class),
            (ServiceModule::class),
            (RepositoryModule::class),
            (ManagerModule::class),
            (DatabaseModule::class)
        ]
)
interface AppComponent : AndroidInjector<MvvmArchApplication> {

    override fun inject(application: MvvmArchApplication)

    @Component.Builder
    interface Builder {

        @BindsInstance
        fun application(application: MvvmArchApplication): Builder

        fun build(): AppComponent
    }
}