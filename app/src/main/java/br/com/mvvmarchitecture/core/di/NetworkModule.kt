package br.com.mvvmarchitecture.core.di

import br.com.mvvmarchitecture.BuildConfig
import br.com.mvvmarchitecture.repository.car.remote.CarEndPoint
import br.com.mvvmarchitecture.repository.core.ApiFactory
import br.com.mvvmarchitecture.repository.core.LiveDataCallAdapterFactory
import com.squareup.moshi.Moshi
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module
class NetworkModule {

    companion object {
        val URL_MOCK = "https://desafiobrq.herokuapp.com/"
        val URL_PRD = ""
    }

    @Singleton
    @Provides
    fun provideMoshi() = Moshi.Builder().build()

    @Singleton
    @Provides
    fun createHttpClient(): OkHttpClient {
        val client = OkHttpClient().newBuilder()

        if (BuildConfig.DEBUG) {
            client.addInterceptor(HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
        }

        client.connectTimeout(30, TimeUnit.SECONDS)
        client.readTimeout(30, TimeUnit.SECONDS)
        client.writeTimeout(30, TimeUnit.SECONDS)
        return client.build()
    }

    @Singleton
    @Provides
    fun provideRetrofitBuilder(okHttpClient: OkHttpClient, moshi: Moshi) = Retrofit.Builder()
            .addConverterFactory(MoshiConverterFactory.create(moshi))
//            .addCallAdapterFactory(LiveDataCallAdapterFactory())
//            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .client(okHttpClient)

    @Provides
    @Singleton
    fun provideApiFactory(retrofitBuilder: Retrofit.Builder): ApiFactory {
        return ApiFactory(retrofitBuilder)
    }

    @Provides
    fun provideCarEndPoint(retrofitBuilder: Retrofit.Builder): CarEndPoint {
        return retrofitBuilder.baseUrl(URL_MOCK).build().create(CarEndPoint::class.java)
    }
}