package br.com.mvvmarchitecture.core.extension

import android.arch.lifecycle.LifecycleOwner
import android.arch.lifecycle.LiveData
import android.arch.lifecycle.Observer

inline fun <T> LifecycleOwner.observeLiveData(data: LiveData<T>, crossinline onChanged: (T) -> Unit) {
  data.observe(this, Observer {
    it?.let { value -> onChanged(value) }
  })
}
