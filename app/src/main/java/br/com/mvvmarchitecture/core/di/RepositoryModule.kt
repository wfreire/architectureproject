package br.com.mvvmarchitecture.core.di

import br.com.mvvmarchitecture.repository.CarRepository
import br.com.mvvmarchitecture.repository.car.local.CarDao
import br.com.mvvmarchitecture.repository.car.remote.CarService
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class RepositoryModule {

    @Singleton
    @Provides
    fun providesCarRepository(carService: CarService, carDao: CarDao) = CarRepository(carService, carDao)
}