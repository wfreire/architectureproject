package br.com.mvvmarchitecture.core.di

import android.app.Application
import br.com.mvvmarchitecture.MvvmArchApplication
import dagger.Binds
import dagger.Module

@Module
abstract class ApplicationModule {

    @Binds
    abstract fun application(app: MvvmArchApplication): Application
}