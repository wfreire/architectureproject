package br.com.mvvmarchitecture.core.di

import android.arch.persistence.room.Room
import br.com.mvvmarchitecture.MvvmArchApplication
import br.com.mvvmarchitecture.repository.car.local.CarDao
import br.com.mvvmarchitecture.repository.core.AppDatabase
import dagger.Module
import dagger.Provides
import javax.inject.Singleton


@Module
class DatabaseModule {

    @Singleton
    @Provides
    fun providesRoomDatabase(application: MvvmArchApplication): AppDatabase {
        return Room.databaseBuilder(application, AppDatabase::class.java, "car-db").build()
    }

    @Singleton
    @Provides
    fun providesCarDao(appDatabase: AppDatabase): CarDao {
        return appDatabase.carDao()
    }
}