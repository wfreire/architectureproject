package br.com.mvvmarchitecture.core.di

import br.com.mvvmarchitecture.repository.car.remote.CarEndPoint
import br.com.mvvmarchitecture.repository.car.remote.CarService
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class ServiceModule {

    @Provides
    @Singleton
    fun provideCarService(carEndPoint: CarEndPoint): CarService {
        return CarService(carEndPoint)
    }
}