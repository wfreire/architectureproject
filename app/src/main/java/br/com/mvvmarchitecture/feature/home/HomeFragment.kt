package br.com.mvvmarchitecture.feature.home

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.annotation.Nullable
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import br.com.mvvmarchitecture.R
import br.com.mvvmarchitecture.core.util.Status
import dagger.android.support.DaggerFragment
import javax.inject.Inject

class HomeFragment : DaggerFragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private val homeViewModel by lazy {
        ViewModelProviders.of(this, viewModelFactory).get(HomeViewModel::class.java)
    }

    override fun onCreateView(inflater: LayoutInflater,
                              @Nullable container: ViewGroup?,
                              @Nullable savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.user_profile, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        homeViewModel.requestCars()
        bindObservers()
    }

    private fun bindObservers() {
        homeViewModel.car.observe( this, Observer {resource ->
            resource?.let {
                when(it.status) {
                    Status.SUCCESS -> Log.e("resource","SUCCESS")
                    Status.ERROR -> Log.e("resource","ERROR")
                    Status.LOADING -> Log.e("resource","LOADING")
                }
            }
        })
    }
}