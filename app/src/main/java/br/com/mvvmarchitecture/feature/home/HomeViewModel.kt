package br.com.mvvmarchitecture.feature.home

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import br.com.mvvmarchitecture.core.util.Resource
import br.com.mvvmarchitecture.repository.CarRepository
import br.com.mvvmarchitecture.repository.car.data.entity.Car
import javax.inject.Inject

class HomeViewModel @Inject constructor(private val carRepository: CarRepository) : ViewModel() {

    lateinit var car: MutableLiveData<Resource<List<Car>>>

    fun requestCars() {
        car = carRepository.getCars(false)
    }
}