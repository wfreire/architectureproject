package br.com.mvvmarchitecture.feature.main

import android.os.Bundle
import br.com.mvvmarchitecture.R
import br.com.mvvmarchitecture.feature.home.HomeFragment
import dagger.android.support.DaggerAppCompatActivity

class MainActivity : DaggerAppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        openHomeFragment()
    }

    fun openHomeFragment() {
        val manager = supportFragmentManager
        val transaction = manager.beginTransaction()
        transaction.add(R.id.main, HomeFragment(), HomeFragment::class.java.simpleName)
        transaction.addToBackStack(null)
        transaction.commit()
    }
}
